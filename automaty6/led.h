	void LedInit(void);
	void LedOn(unsigned char ucLedIndeks);
	void LedOff(unsigned char ucLedIndeks);
	void LedStepLeft(void);
	void LedStepRight(void);
