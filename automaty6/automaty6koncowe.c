#include <LPC21xx.H>
#include "led.h"
#include "keyboard.h"

void Delay(int iMiliSeconds){
	int dTime;
	int x=iMiliSeconds*2380;
	for(dTime = 0; dTime<x ; dTime++){	
	}
}

int main(){
	
	enum LedState{LED_LEFT,LED_RIGHT,LED_STOP};
	enum LedState  eLedState = LED_STOP;

	LedInit();
	KeyboardInit();

	while(1){
		switch(eLedState){
			case LED_RIGHT: 	
				if(eKeyboardRead()==BUTTON_1){
					eLedState = LED_STOP;
				}
				else{
					eLedState = LED_RIGHT;
					LedStepRight();
				}
				break;
			case LED_LEFT: 
				if(eKeyboardRead()==BUTTON_1){
					eLedState = LED_STOP;
				}
				else{
					eLedState = LED_LEFT;
					LedStepLeft();
				}
				break;
			case LED_STOP: 
				if(eKeyboardRead()==BUTTON_0){
					eLedState = LED_LEFT;
				}
				else if(eKeyboardRead()==BUTTON_2){
					eLedState = LED_RIGHT;
				}
				else{
					eLedState = LED_STOP;	
				}
				break;
	 }
	 Delay(100);
	}
}
